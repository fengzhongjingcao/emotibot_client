# 竹间智能机器人客户端

#### 介绍
演示Emotibot API调用

#### 使用说明
1.  安装libcurl-dev软件包，`apt install libcurl4-gnutls-dev`
2.  将`emotibot.c`中的`APPID`和`USERID`替换成自己的。
3.  make

API文档：http://console.developer.emotibot.com/api/ApiKey/documentation.php