#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "cJSON.h"

#define APPKEY "rKCHBLmYiFPuCQTS0HttLbUD"
#define SECRETKEY "037dc446820ec143d1628c20146b9d34"

extern CURL* curl;

char* get_token(const char* appkey, const char* secretkey)
{
	char* request;
	char* response;
	size_t resplen;

	asprintf(&request, "grant_type=client_credentials&client_id=%s&client_secret=%s", appkey, secretkey);
	FILE* fp = open_memstream(&response, &resplen);
	if (!fp)
		perror("open_memstream");
	curl_easy_setopt(curl, CURLOPT_URL, "http://openapi.baidu.com/oauth/2.0/token");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request);	
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);	
	CURLcode res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		puts(curl_easy_strerror(res));
	fclose(fp);

	cJSON* resp = cJSON_Parse(response);
	//puts(cJSON_Print(resp));
	cJSON* token = cJSON_GetObjectItemCaseSensitive(resp, "access_token");

	char* retval = strdup(token->valuestring);
	
	free(response);
	cJSON_Delete(resp);

	return retval;
}

void tts_run(char* input)
{
	char* request;
	char* token = get_token(APPKEY, SECRETKEY);
	char* temp = curl_easy_escape(curl, input, strlen(input));
	char* text = curl_easy_escape(curl, temp, strlen(temp));
	curl_free(temp);
	asprintf(&request, "ctp=1&lan=zh&cuid=1234&tok=%s&tex=%s&per=0&spd=5&pit=5&vol=5&aue=6", token, text);
	//puts(request);

	FILE* fp = fopen("tts.wav", "w+");
	if (!fp)
		perror("fopen");
	curl_easy_setopt(curl, CURLOPT_URL, "http://tsn.baidu.com/text2audio");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request);	
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);	
	CURLcode res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		puts(curl_easy_strerror(res));
	fclose(fp);
	curl_free(text);
	free(token);
	system("aplay -q tts.wav");
}
