#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "cJSON.h"

#define APPID "22b4a23787f0f249b1c8f3a04ade8c09"
#define USERID "liuyu"

CURL* curl;

extern void tts_run(char* input);

int emotibot_init()
{
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	return 0;
}

void emotibot_cleanup()
{
	curl_easy_cleanup(curl);
	curl_global_cleanup();
}

void emotibot_chat(char* input)
{
	char* request;
	char* response;
	size_t resplen;

	asprintf(&request, "cmd=chat&appid=%s&userid=%s&text=%s", APPID, USERID, input);
	//puts(request);

	FILE* fp = open_memstream(&response, &resplen);
	if (!fp)
		perror("open_memstream");
	curl_easy_setopt(curl, CURLOPT_URL,
			"http://idc.emotibot.com/api/ApiKey/openapi.php");
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request);	
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);	
	CURLcode res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		puts(curl_easy_strerror(res));
	fclose(fp);

	cJSON* resp = cJSON_Parse(response);
	//puts(cJSON_Print(resp));

	cJSON* retval = cJSON_GetObjectItemCaseSensitive(resp, "return");
	if (retval->valueint)
	{
		cJSON* retmsg = cJSON_GetObjectItemCaseSensitive(resp, "return_message");
		puts(retmsg->valuestring);
		return;
	}

	cJSON* data = cJSON_GetObjectItemCaseSensitive(resp, "data");
	cJSON* obj;

	cJSON_ArrayForEach(obj, data)
	{
		cJSON* type = cJSON_GetObjectItemCaseSensitive(obj, "type");
		if (!strcmp(type->valuestring, "text"))
		{
			cJSON* value = cJSON_GetObjectItemCaseSensitive(obj, "value");
			printf("> %s\n", value->valuestring);
			tts_run(value->valuestring);
		}
	}

	free(response);
	cJSON_Delete(resp);
}

int main()
{
	char inputbuf[512];

	emotibot_init();
	while(fgets(inputbuf, sizeof inputbuf, stdin))
	{
		emotibot_chat(inputbuf);
	}
	emotibot_cleanup();

	return 0;
}

