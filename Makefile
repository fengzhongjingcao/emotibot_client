.PHONY: all clean

CFLAGS = -Wall -D_GNU_SOURCE
LDFLAGS = -lcurl

all: emotibot

emotibot: emotibot.c baidu_tts.c cJSON.c

clean:
	$(RM) emotibot
